// JavaScript Document

var EID = EID || {};

EID.Site = {
	

	// initialize functions on pageload

	init: function() {
		EID.Site.articleHover(); 
		EID.Site.orgAnimation();

		// resize
		$(window).resize(EID.Site.docResize);

		// scroll
		$(window).scroll(EID.Site.docScroll);

		// imageLoad
		$('img').load(EID.Site.imageLoad);

	},

	// initialize functions on other triggers

	docResize: function() {
		EID.Site.orgAnimation();
	},

	docScroll: function() {
		EID.Site.orgAnimation();
	},

	imageLoad: function() {
	},

	// functions

	articleHover: function() {
		var article = $('.art1');
		article.each(function() {
			$(this).find('.button').hover(function() {
				$(this).parents('article').addClass('hover');
			}, function() {
				$(this).parents('article').removeClass('hover');
			});
		});
	},

	orgAnimation: function() {
		var topDist = 200;
		var org = $('.org');
		org.each(function() {
			var windowTop = $(window).scrollTop();
			var windowWidth = $(window).width();
			var parent = $(this).parents('.about-animation');
			var parentTop = parent.offset().top;
			var parentLeft = parent.offset().left;
			var parentWidth = parent.outerWidth();
			var parentRight = windowWidth - (parentLeft + parentWidth);
			if (windowTop >= (parentTop - topDist)) {
				$(this).addClass('fixed');
				$(this).css('top', topDist).css('right', parentRight);
				if ($(this).hasClass('org2')) {
					$('.org1').addClass('removed');
				}
			}
			else {
				$(this).removeClass('fixed');
			 	$(this).css('top', 0).css('right', 0);
				if ($(this).hasClass('org2')) {
					$('.org1').removeClass('removed');
				}
			}
		});
	}
	
};

$(document).ready(EID.Site.init);
